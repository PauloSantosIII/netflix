import React from 'react';
import { FooterBase } from './styles';
import LogoPSIII from '../../assets/img/LogoPSIII.png'

function Footer() {
  return (
    <FooterBase>
      <a href="/">
        <img className='LogoPSIII' src={LogoPSIII} alt='Logo PauloSantosIII' />
      </a>
      <p>
        Orgulhosamente criado durante a
        {' '}
        <a href="https://davichris-git-master.paulosantosiii.vercel.app/">
          Imersão React da Alura
        </a>
      </p>
    </FooterBase>
  );
}

export default Footer;
